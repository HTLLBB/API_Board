using System;
using System.Threading.Tasks;
using Board.Controllers;
using Board.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace Board.Tests
{
    public class CategoryControllerTests
    {
        public CategoryControllerTests()
        {
            _ctx = new BoardContext(
                new DbContextOptionsBuilder<BoardContext>()
                    .UseInMemoryDatabase("BoardTests")
                    .Options
            );
            _ctrl = new CategoryController(_ctx);
        }

        private readonly BoardContext _ctx;
        private readonly CategoryController _ctrl;

        [Fact]
        public async Task GetCategories()
        {
            // Arrange
            // Act
            var res = await _ctrl.GetAll();

            // Assert
            Assert.NotNull(res);
        }

        [Fact]
        public async Task GetCategoriesCount()
        {
            // Arrange
            for (var i = 0; i < 10; i++)
                await _ctx.Categories.AddAsync(
                    new Category { Name = $"GetCategoriesCount{i}" });
            await _ctx.SaveChangesAsync();
            
            // Act
            var res = await _ctrl.GetAll();
            
            // Assert
            Assert.NotNull(res);
            Assert.NotEmpty(res);
            Assert.Equal(10, res.Count);
        }

        [Fact]
        public async Task PostValidCategory()
        {
            // Arrange
            var cat = new Category
            {
                Name = "PostCategoryAsync",
            };

            // Act
            var res = await _ctrl.Create(cat) as CreatedAtRouteResult;
            var given_cat = res.Value as Category;

            // Assert
            Assert.NotNull(res);
            Assert.Equal(201, res.StatusCode);
            Assert.False(string.IsNullOrWhiteSpace(given_cat.Id));
        }

        [Fact]
        public async Task PostDuplicateCategory()
        {
            // Arrange
            var cat = new Category
            {
                Name = "PostDuplicateCategoryAsync"
            };
            await _ctrl.Create(cat);

            // Act
            var res = await _ctrl.Create(cat) as BadRequestObjectResult;

            // Assert
            Assert.NotNull(res);
            Assert.Equal(400, res.StatusCode);
            Assert.NotNull(res.Value);
        }

        [Fact]
        public async Task PostEmptyCategory()
        {
            // Arrange
            var cat = new Category();

            // Act
            var res = await _ctrl.Create(cat) as BadRequestObjectResult;

            // Assert
            Assert.NotNull(res);
            Assert.Equal(400, res.StatusCode);
            Assert.NotNull(res.Value);
        }

        [Fact]
        public async Task DeleteCategories()
        {
            // Arrange
            for (var i = 0; i < 10; i++)
                await _ctx.Categories.AddAsync(
                    new Category { Name = $"DeleteCategoriesAsync{i}" });
            await _ctx.SaveChangesAsync();
            Assert.NotEmpty(_ctx.Categories);
            
            // Act
            var res = await _ctrl.Delete();
            
            // Assert
            Assert.NotNull(res);
            Assert.Equal(204, res.StatusCode);
            Assert.Empty(_ctx.Categories);
        }

        [Fact]
        public async Task GetExistingCategory()
        {
            // Arrange
            var cat = new Category()
            {
                Name = "GetExistingCategoryAsync"
            };
            await _ctx.Categories.AddAsync(cat);
            await _ctx.SaveChangesAsync();

            // Act
            var res = (await _ctrl.GetById(cat.Id)).Value;
            
            // Assert
            Assert.NotNull(res);
            Assert.Equal(cat.Name, res.Name);
        }

        [Fact]
        public async Task GetInexistantCategory()
        {
            // Arrange
            string id;
            do id = Guid.NewGuid().ToString();
            while (await _ctx.Categories.AnyAsync(c => c.Id == id));
            
            // Act
            var res = (await _ctrl.GetById(id)).Result as NotFoundResult;

            // Assert
            Assert.NotNull(res);
            Assert.Equal(404, res.StatusCode);
        }

        [Fact]
        public async Task PutValidCategory()
        {
            // Arrange
            var cat = new Category
            {
                Name = "PutValidCategory"
            };
            await _ctx.Categories.AddAsync(cat);
            await _ctx.SaveChangesAsync();
            cat.Name = "PutValidCategory2";
            
            // Act
            var res = await _ctrl.Update(cat.Id, cat) as NoContentResult;
            
            // Assert
            Assert.NotNull(res);
            Assert.Equal(204, res.StatusCode);
        }

        [Theory]
        [InlineData("PutInvalidCategory")]
        [InlineData("PutInvalidCategory2")]
        public async Task PutInvalidCategory(string name)
        {
            // Arrange
            var cat = new Category { Name = "PutInvalidCategory" };
            await _ctx.Categories.AddAsync(cat);
            await _ctx.Categories.AddAsync(
                new Category {Name = "PutInvalidCategory2"});
            await _ctx.SaveChangesAsync();
            
            // Act
            var res = await _ctrl.Update(cat.Id,
                new Category { Name = name }) as BadRequestObjectResult;
            
            // Assert
            Assert.NotNull(res);
            Assert.Equal(400, res.StatusCode);
            Assert.NotNull(res.Value); // err msg
        }

        [Fact]
        public async Task PutInexistantCategory()
        {
            // Arrange
            string id;
            do id = Guid.NewGuid().ToString();
            while (await _ctx.Categories.AnyAsync(c => c.Id == id));
            
            // Act
            var res = await _ctrl.Update(
                    id, new Category {Name = "PutInexistantCategory"}) 
                as BadRequestObjectResult;
            
            // Assert
            Assert.NotNull(res);
            Assert.Equal(400, res.StatusCode);
            Assert.NotNull(res.Value); // err msg
        }

        [Fact]
        public async Task DeleteValidCategory()
        {
            // Arrange
            var cat = new Category
            {
                Name = "DeleteValidCategory"
            };
            await _ctx.Categories.AddAsync(cat);
            await _ctx.SaveChangesAsync();
            
            // Act
            var res = await _ctrl.Delete(cat.Id) as NoContentResult;

            // Assert
            Assert.NotNull(res);
            Assert.Equal(204, res.StatusCode);
            Assert.DoesNotContain(_ctx.Categories, c => c.Id == cat.Id);
        }

        [Fact]
        public async Task DeleteInexistantCategory()
        {
            // Arrange
            string id;
            do id = Guid.NewGuid().ToString();
            while (await _ctx.Categories.AnyAsync(c => c.Id == id));
            
            // Act
            var res = await _ctrl.Delete(id) as NotFoundResult;
            
            // Assert
            Assert.NotNull(res);
        }
    }
}