using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Board.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.EntityFrameworkCore;

using Id = System.String;

namespace Board.Controllers
{
    [ApiController]
    [Route("Board/[controller]")]
    public class CategoryController : ControllerBase
    {
        private readonly BoardContext _ctx;

        public CategoryController(BoardContext ctx)
        {
            _ctx = ctx;
        }

        [HttpGet]
        [ProducesResponseType(200)]
        public async Task<List<Category>> GetAll()
        {
            return await _ctx.Categories.ToListAsync();
        }

        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Create([FromBody] Category category)
        {
            TryValidateModel(category);
            
            if (await _ctx.Categories.AnyAsync(c => c.Name == category.Name))
                return BadRequest(new {Name = "Already taken"});

            if (string.IsNullOrWhiteSpace(category.Name))
                return BadRequest(new {Name = "Must not be empty"});

            await _ctx.Categories.AddAsync(category);
            await _ctx.SaveChangesAsync();

            return CreatedAtRoute("GetCategory", new {id = category.Id}, category);
        }

        [HttpDelete]
        [ProducesResponseType(204)]
        public async Task<NoContentResult> Delete()
        {
            _ctx.RemoveRange(_ctx.Categories);
            return NoContent();
        }

        [HttpGet("{id}", Name = "GetCategory")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<Category>> GetById(string id)
        {
            var cat = await _ctx.Categories.SingleOrDefaultAsync(c => c.Id == id);
            if (cat is null) 
                return NotFound();

            return cat;
        }

        [HttpPut("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Update(Id id, Category item)
        {
            throw new NotImplementedException();
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Delete(Id id)
        {
            throw new NotImplementedException();
        }
    }
}