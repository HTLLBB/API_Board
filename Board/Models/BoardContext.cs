using System;
using Microsoft.EntityFrameworkCore;

namespace Board.Models
{
    public class BoardContext : DbContext
    {
        public BoardContext(DbContextOptions options): base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Forum> Forums { get; set; }
        public DbSet<Thread> Threads { get; set; }
        public DbSet<Post> Posts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }
    }
}