using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Board.Models
{
    public class Category
    {
        [BindNever]
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }

        public List<Forum> Forums { get; set; }
    }
}