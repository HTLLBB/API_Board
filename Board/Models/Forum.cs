using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Board.Models
{
    public class Forum
    {
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }

        public List<Thread> Threads { get; set; }
    }
}