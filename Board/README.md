# Board Microservice

This api defines the following RESTful endpoints.

---

## GET Board/Category | Void -> Category List

Returns the list of categories. Status 200 OK.

## POST Board/Category | Category -> Id

[ _authenticate_ ]

Adds a new category with attributes (Name) taken from the JSON body, return its Id. Status 201 Created || 400 Bad Request.

Name must be unique.

## DELETE Board/Category | Void -> Void

[ _authenticate_; _authorize_ ]

Deletes all the categories. Status 204 No Content.

---

## GET Board/Category/:id | Id -> Category

Returns the category with the given Id. Status 200 OK || 404 Not Found.

## PUT Board/Category/:id | Id -> Category -> Void

[ _authenticate_; _authorize_ ]

Updates the category with the given Id, with attributes (Name) taken from the JSON body. Status 204 No Content || 400 Bad Request || 404 Not Found.

Name must be unique.

## DELETE Board/Category/:id | Id -> Void

[ _authenticate_; _authorize_ ]

Deletes the category referenced by the given Id. Status 204 No Content || 404 Not Found.

---

## GET Board/Forum | Void -> Forum List

Returns the list of forums. Status 200 OK.

## POST Board/Forum | Forum -> Id

[ _authenticate_ ]

Adds a new forum with attributes (Name, CategoryId) taken from the JSON body, return its Id. Status 201 Created || 400 Bad Request.

Name must be unique.

## DELETE Board/Forum | Void -> Void

[ _authenticate_; _authorize_ ]

Deletes all the forums. Status 204 No Content.

---

## GET Board/Forum/:id | Id -> Forum

Returns the forum with the given Id. Status 200 OK || 404 Not Found.

## PUT Board/Forum/:id | Id -> Forum -> Void

[ _authenticate_; _authorize_ ]

Updates the forum with the given Id, with attributes (Name, CategoryId) taken from the JSON body. Status 204 No Content || 404 Not Found.

## DELETE Board/Forum/:id | Id -> Void

[ _authenticate_; _authorize_ ]

Deletes the forum referenced by the given Id. Status 204 No Content || 404 Not Found.

---

## GET Board/Thread | Void -> Thread List

Returns the list of threads. Status 200 OK.

## POST Board/Thread | Thread -> Id

[ _authenticate_ ]

Adds a new thread with attributes (Title, Op, ForumId) taken from the JSON body, return its Id. Status 201 Created || 400 Bad Request.

Title must be unique.

## DELETE Board/Thread | Void -> Void

[ _authenticate_; _authorize_ ]

Deletes all the threads. Status 204 No Content.

---

## GET Board/Thread/:id | Id -> Thread

Returns the thread with the given Id. Status 200 OK || 404 Not Found.

## PUT Board/Thread/:id | Id -> Thread -> Void

[ _authenticate_; _authorize_ ]

Updates the thread with the given Id, with attributes (Title, Op, ForumId) taken from the JSON body. Status 204 No Content || 404 Not Found.

## DELETE Board/Thread/:id | Id -> Void

[ _authenticate_; _authorize_ ]

Deletes the thread referenced by the given Id. Status 204 No Content || 404 Not Found.

---

## GET Board/Post | Void -> Post List

Returns the list of posts. Status 200 OK.

## POST Board/Post | Post -> Id

[ _authenticate_ ]

Adds a new post with attributes (Content, ThreadId) taken from the JSON body, return its Id. Status 201 Created || 400 Bad Request.

## DELETE Board/Post | Void -> Void

[ _authenticate_; _authorize_ ]

Deletes all the posts. Status 204 No Content.

---

## GET Board/Post/:id | Id -> Post

Returns the post with the given Id. Status 200 OK || 404 Not Found.

## PUT Board/Post/:id | Id -> Post -> Void

[ _authenticate_; _authorize_ ]

Updates the post with the given Id, with attributes taken from the JSON body. Status 204 No Content || 404 Not Found.

## DELETE Board/Post/:id | Id -> Void

[ _authenticate_; _authorize_ ]

Deletes the post referenced by the given Id. Status 204 No Content || 404 Not Found.